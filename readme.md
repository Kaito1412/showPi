showIP
======

### Configuración

* Habilitar login
* Usuario
* Contraseña
* Puerto
* Lista de procesos monitorizables
* Lista de logs a mostar

### Resumen

* Sistema operativo ✓
* Distribución ✓
* Kernel ✓
* Porcentaje CPU ✓
* USo Memoria (Porcentaje)
* Disco usado de Disco total (Porcentaje)

### Sistema operativo

* Sistema operativo ✓
* Distibución ✓
* Kernel ✓
* Shell ✓
* Usuario ✓
* Hostname ✓
* Uptime

### CPU ✓

* Fabricante ✓
* Modelo ✓
* Número de CPUs ✓
* Porcentaje usado ✓
* Herzios de cada una CPU ✓
* Temperatura ✓

### GPU

### Memoria

* Memoria total ✓
* Usada (Porcentaje) ✓
* Swap (Si hay) ✓
  * Total ✓
  * Usada (Porcentaje) ✓

### Discos

Lista de discos y particiones con:

* Ruta Físcica (en el disco o en la red) ✓
* Ruta de montaje ✓
* Sistema de ficheros ✓
* Espacio libre (porcentaje) ✓
* Total ✓

### Procesos

Lista de procesos que se hayan configurado para monitorear
* Pid
* Nombre
* Uso CPU/Memoria

### Versión programas

Lista con los programas que se desean mostrar y su versión
* Nombre
* Versión

### Logs

Ver logs puestos de los archivos puestos en el log de configuracion

### Red

* IP pública
* IP privada
* Netmask
* Gateway
* Interfaces
* Velocidades
* Lista de nodos conectados a la red

### Bluethooth

Lista de dispositivos disponibles

### Sensores

* Temperatura
* Humedad
* Sonido
* Cámara
  * Streaming (por defecto)
  * Foto
  * Video

### Notificaciones

* Temperatura CPU muy alta
* CPU a tope
* RAM a tope
* Poco espacio
