from datetime import timedelta
import os
import platform

class OS:
    
    name = platform.system()
    distribution = platform.dist()[0]
    architecture = platform.architecture()[0]
    hostname = platform.node()
    kernel = platform.release()
    hostname = platform.node()
    user = os.environ['USER']
    shell = os.environ['SHELL']
    terminal = os.environ['TERM']

    @classmethod
    def uptime(self):
        with open('/proc/uptime', 'r') as f:
           uptime_seconds = float(f.readline().split()[0])
           return str(timedelta(seconds=uptime_seconds))
