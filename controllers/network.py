import netifaces

class Network:

    @classmethod
    def interfaces(self):
        interfaces = []
        for interface in netifaces.interfaces():
            netInfo = netifaces.ifaddresses(interface)
            if 2 in netInfo:
                netInfo[2][0]["active"] = True
                interfaces.append({"name": interface, "data": netInfo[2][0]})
            else:
                interfaces.append({"name": interface, "data": {"active": False}})
        return interfaces
