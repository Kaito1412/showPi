import psutil
import math

class Disk:
    
    disks = {} 
    
    @classmethod
    def all(self):
        self.disks = {"total": 0, "used": 0, "partitions": []}
        for disk in psutil.disk_partitions():
            self.disks["partitions"].append(self.one(disk))

        self.disks["percent"] = (self.disks["used"] / self.disks["total"]) * 100
        self.disks["used"] = math.trunc(self.disks["used"] / 1073741824)
        self.disks["total"] = math.trunc(self.disks["total"] / 1073741824)
        return self.disks
    
    @classmethod
    def one(self, disk):
        disk_usage = psutil.disk_usage(disk.mountpoint)
        self.disks["total"] += disk_usage.total
        self.disks["used"] += disk_usage.used
        return {"mountpoint": disk.mountpoint,
                "fs": disk.fstype,
                "device": disk.device,
                "total": math.trunc(disk_usage.total / 1073741824),
                "used": math.trunc(disk_usage.used / 1073741824),
                "percent": disk_usage.percent,
                }
