import psutil

class Memory:
    
    total = psutil.virtual_memory().total
    swap_total= psutil.swap_memory().total

    @classmethod
    def used(self):
        return psutil.virtual_memory().used

    @classmethod
    def percent(self):
        return psutil.virtual_memory().percent

    @classmethod
    def swap_used(self):
        return psutil.swap_memory().used

    @classmethod
    def swap_percent(self):
        return psutil.swap_memory().percent
