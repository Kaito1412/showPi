from cpuinfo import cpuinfo
import psutil
import platform

class CPU:
    
    vendor = cpuinfo.get_cpu_info()['vendor_id'] 
    model = cpuinfo.get_cpu_info()['brand']
    number = psutil.cpu_count(logical=True)
    total = cpuinfo.get_cpu_info()['hz_advertised']

    @classmethod
    def used(self):
        return psutil.cpu_percent(percpu=True)

    @classmethod
    def temperature(self):
        sysFile = open( "/sys/class/thermal/thermal_zone0/temp" )
        cpu_temp = sysFile.read()
        sysFile.close()
        return float(cpu_temp) / 1000
