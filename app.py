#!Python

import config
import lesscpy
from bottle import route, run, response, hook, static_file, template
from controllers.os import OS
from controllers.cpu import CPU
from controllers.memory import Memory 
from controllers.disk import Disk
from controllers.network import Network

@hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

@route('/')
def send_static():
    return template('public/index.html')

@route('/<static:path>')
def send_static(static):
    return static_file(static, root='public')

@route('/api/all')
def get():
    return {'os': {
                'name': OS.name,
                'dist': OS.distribution,
                'kernel': OS.kernel,
                'architecture': OS.architecture,
                'hostname': OS.hostname,
                'user': OS.user,
                'shell': OS.shell,
                'terminal': OS.terminal,
                'uptime': OS.uptime()
            },
            'cpu': {
                'vendor': CPU.vendor,
                'model': CPU.model,
                'count': CPU.number,
                'used': CPU.used(),
                'total': CPU.total,
                'temperature': CPU.temperature()
            },
            'memory': {
                'memory_used': Memory.used(),
                'memory_percent': Memory.percent(),
                'memory_total': Memory.total,
                'swap_used': Memory.swap_used(),
                'swap_percent': Memory.swap_percent(),
                'swap_total': Memory.swap_total
            },
            'disk': Disk.all(),
            'network': Network.interfaces(),
        }

@route('/api/notify')
def notify():
    return {
            'memory': Memory.used(),
            'swap': Memory.swap_used(),
            'cpu': CPU.used(),
            'temperature': CPU.temperature()
        }

@route('/favicon.ico')
def get_favicon():
    return server_static('public/images/distros/arch.png')

def compileLess():
    css = lesscpy.compile('less/style.less', minify=True)
    file_css = open('public/css/style.css', 'w')
    file_css.write(css)
    file_css.close()

compileLess()
run(host='0.0.0.0', port=config.PORT, reloader=True)
