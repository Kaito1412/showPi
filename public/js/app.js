require(["knockout", "zepto"], function (ko, $) {
  var ViewModel = function(data) {
    this.os = data.os;
    this.cpu = data.cpu;
    this.memory = data.memory;
    this.disk = data.disk;
    this.network = data.network;

    this.changeUnit = function(data, unit) {
       return Math.floor(data / unit);
    };
    
    this.memory.memory_used = this.changeUnit(this.memory.memory_used, 1048576);
    this.memory.memory_total = this.changeUnit(this.memory.memory_total, 1048576);
    this.memory.swap_used = this.changeUnit(this.memory.swap_used, 1048576);
    this.memory.swap_total = this.changeUnit(this.memory.swap_total, 1048576);
    this.disk.used = this.changeUnit(this.disk.used, 1073741824);

    this.showMenu = function() {
      Zepto("header").toggleClass("activeMenu");
    };

    this.changeSection = function (data, event) {
      var currentClass = event.target.text.toLowerCase();
      Zepto('header').removeClass('activeMenu');
      Zepto('.active').removeClass('active');
      Zepto('#' + currentClass).addClass('active');
    };

    this.selectSensor = function (data, event) {
      console.log(event.target);
      var currentID = event.target.id.toLowerCase();
      Zepto('#sensor > div:not(.header)').removeClass();
    };
  };

  Zepto.get(window.location.origin + "/api/all", function(data) {
    ko.applyBindings(new ViewModel(data));
  });
});
