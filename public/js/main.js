requirejs.config({
  "baseUrl": "js/vendor",
  "paths": {
    "knockout": "knockout/dist/knockout",
    "zepto": "zepto/zepto.min",
    "app": "../app",
  }
});

requirejs(["app"]);
